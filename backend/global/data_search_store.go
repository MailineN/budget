package global


type SearchStore struct {
	StoreName   	string  `json:"storeName"`
	StoreType 		string  `json:"storeType"`
	LastAdded       int 	`json:"lastAdded"`
	Count     		int  	`json:"count"`
}

type SearchStoreData struct {
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchStore   	*SearchStore 		`json:"searchStore"`
}

type SearchStoreBody struct {
	User 			*User 				`json:"user"`
	Phone 			*Phone 				`json:"phone"`
	Sync_Order 		int64	     		`json:"syncOrder"`
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchStore   	*SearchStore 		`json:"searchStore"`
}
package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)

// GroupBody
// {
// 	"superuser" : {"name" : "su1n", "password" : "su1p"},
// 	"group" 	: {"name" : "HBS 2020 A"}, 
// 	"groupInfos" 	: [ {"key" : "key1", "value" : "val1A"},
//                  {"key" : "key2", "value" : "val2A"}]
// }

func Group_List(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Group_List")

	var body global.GroupBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserMonitoring(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers, monitoring and casemanagement can list all users!")
		return
	}
	
	var groups []global.Group
	groups, err = database.Select_Groups()
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var groupDatas []global.GroupData

	for i, _ := range groups {
		var groupData global.GroupData

		var group_infos []global.Group_Info
		group_infos, err = database.Select_GroupInfos_ByGroup(groups[i].Id)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		groupData.Group = &groups[i]
		groupData.Group_Infos = group_infos

		groupDatas = append(groupDatas, groupData)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(groupDatas)
}

func Group_Create(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Group_Create")

	var body global.GroupBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var group global.Group	
	group, err = database.Select_Group_ByName(global.GROUP_SUPERUSER)
	if err != nil {
		if body.Group.Name != global.GROUP_SUPERUSER {
			group, err = database.Select_Group_ByName(global.GROUP_CMM)
			if err != nil {
				if body.Group.Name != global.GROUP_CMM {
					badRequest(w, true, "Group '" + global.GROUP_CMM + "' or '" + global.GROUP_SUPERUSER + "' has to be created first!")
					return
				}
			} else {
				body.Superuser.Group_Id = group.Id 
				_, err = database.Select_User_ByNamePasswordGroup(*body.Superuser)
				if err != nil {
					badRequest(w, true, "Only superusers and casemanagement users can create Groups!")
					return
				}
			}

			group, err = database.Select_Group_ByName(body.Group.Name)
			if err == nil {
				badRequest(w, true, "Group '" + body.Group.Name + "' already exists.")
				return
			}
		}
	} else {
		group, err = database.Select_Group_ByName(body.Group.Name)
		if err == nil {
			badRequest(w, true, "Group '" + body.Group.Name + "' already exists.")
			return
		}
	}


	body.Group.Status = global.STATUS_DISABLED
	_, err = database.Create_Group(*body.Group)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	group, err = database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var group_infos []global.Group_Info
	for _, group_info := range body.Group_Infos {
		group_info.Group_Id = group.Id

		_, err = database.Select_GroupInfo_ByGroupIdKey(group_info)
		if err == nil {
			badRequest(w, true, "GroupSetting '" + group_info.Key + "' already exists.")
			return
		}

		_, err = database.Create_GroupInfo(group_info)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		group_info, _ = database.Select_GroupInfo_ByGroupIdKey(group_info)
		group_infos = append(group_infos, group_info)
	}

	var group_data global.GroupData
	group_data.Group = &group 

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(group_data)
}

func Group_Infos(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Group_Infos")

	var body global.GroupBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser)  && !database.IsUserMonitoring(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers, monitoring users and casemanagement users can list Groups!")
		return
	}
	
	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist.")
		return
	}

	err = database.Delete_GroupInfos(group)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var group_infos []global.Group_Info
	for _, group_info := range body.Group_Infos {
		group_info.Group_Id = group.Id

		_, err = database.Select_GroupInfo_ByGroupIdKey(group_info)
		if err == nil {
			badRequest(w, true, "GroupSetting '" + group_info.Key + "' already exists.")
			return
		}

		_, err = database.Create_GroupInfo(group_info)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}

		group_info, _ := database.Select_GroupInfo_ByGroupIdKey(group_info)
		group_infos = append(group_infos, group_info)
	}

	var group_data global.GroupData
	group_data.Group = &group
	group_data.Group_Infos = group_infos

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(group_data)
}

func Group_Enable(w http.ResponseWriter, r *http.Request) {
	setGroupStatus(w, r, global.STATUS_ENABLED)
}

func Group_Disable(w http.ResponseWriter, r *http.Request) {
	setGroupStatus(w, r, global.STATUS_DISABLED)
}

func setGroupStatus(w http.ResponseWriter, r *http.Request, status string) {
	global.MyPrint("#")
	global.MyPrint("setGroupStatus")

	var body global.GroupBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers, and casemanagement users can change a Group's status!")
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	body.Group.Id = group.Id
	body.Group.Status = status
	err = database.Update_Group(*body.Group)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	group, err = database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(group)
}


func Group_Delete(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Group_Delete")

	var body global.GroupBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and casemanagement users can delete Groups!")
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	err = database.Delete_Group(group.Id)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' can not be deleted while it contains users or infos!")
		return
	}

	group.Status = "DELETED"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(group)
}

func Group_Rename(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Group_Rename")

	var body global.UserBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserCasemanagement(*body.Superuser) {
		badRequest(w, true, "Only superusers and casemanagement users can rename Groups!")
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	_, err = database.Select_Group_ByName(body.Regroup.Name)
	if err == nil {
		badRequest(w, true, "Group '" + body.Regroup.Name + "' already exists.")
		return
	}

	group.Name = body.Regroup.Name
	err = database.Update_Group(group)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(group)
}

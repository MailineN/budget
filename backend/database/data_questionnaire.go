package database

import (
	_ "github.com/lib/pq"
	"backend/global"
)

// ### ACTIVITY_DATA ######################################################################################

func PushNextQuestionnaireData(body global.StartQuestionnaireBody) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errI error

	user, _ = Select_User_ById(body.Synchronisation.User_Id)
	phone, _ = Select_Phone_ById(body.Synchronisation.Phone_Id)

	var pushed int64 = body.Synchronisation.Sync_Order
	var latest int64 = user.Sync_Order
	latest = latest + 1
	body.Synchronisation.Sync_Order = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = Select_Synchronisation_ByDataIdentifier(tx,
			body.Synchronisation.Data_Type,
			body.Synchronisation.Data_Identifier, body.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = Create_Synchronisation(tx, body.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			body.Synchronisation.Id = synchronisation.Id
			updated, err = Update_Synchronisation(tx, body.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errI = Delete_Start_Questionnaire(tx, sync_id)
					if errI != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errI = Create_Start_Questionnaire(tx, sync_id, body.StartQuestionnaire)
			}
		}

		if validTransaction {
			user.Sync_Order = latest
			err = Update_UserSyncOrder(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Sync_Order = pushed
				err = Update_Phone(tx, phone)
				if err != nil {

					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextQuestionnaireData(user_id int64, phone_id int64, pulled_sync_order int64) (global.StartQuestionnaireData, error) {
	var synchronisation global.Synchronisation
	var startQuestionnaire global.StartQuestionnaire

	synchronisation, err = Select_Synchronisation_NextToPull(global.DATA_QUESTIONNAIRE, user_id, phone_id, pulled_sync_order)

	if synchronisation.Action != global.ACTION_DELETE {
		startQuestionnaire, _ = Select_Start_Questionnaire(synchronisation.Id)
	}

	var data global.StartQuestionnaireData
	data.Synchronisation = &synchronisation
	data.StartQuestionnaire = &startQuestionnaire

	return data, nil
}

func GetQuestionnaireData(sync_id int64) global.StartQuestionnaireData {
	var synchronisation global.Synchronisation
	var startQuestionnaire global.StartQuestionnaire

	synchronisation, _ = Select_Synchronisation_ById(sync_id)
	startQuestionnaire, _ = Select_Start_Questionnaire(synchronisation.Id)

	var data global.StartQuestionnaireData
	data.Synchronisation = &synchronisation
	data.StartQuestionnaire = &startQuestionnaire

	return data
}

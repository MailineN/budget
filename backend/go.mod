module backend

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.2.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
)

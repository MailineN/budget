//
//  ScanResult.swift
//  Runner
//
//  Created by Marc Wiggerman on 26/03/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation
import UIKit

/// Represent a complete scan result containing OCR results, a cropped image and an original image
struct ScanResult: Codable {
    var visionResult: VisionResult!

    /// The size of the image used in the OCR
    var croppedImageSize: VisionResult.VisionSize
    /// The cropped image
    var croppedImageBase64: String!


    /// The size of the image used in the OCR
    var originalImageSize: VisionResult.VisionSize
    /// The original uncropped image
    var originalImageBase64: String!

    init (visionResult: VisionResult, croppedImage: UIImage, originalImage: UIImage) {
        self.visionResult = visionResult

        self.croppedImageSize = VisionResult.VisionSize(size: croppedImage.size)
        self.croppedImageBase64 = croppedImage.base64Encoded

        self.originalImageSize = VisionResult.VisionSize(size: originalImage.size)
        self.originalImageBase64 = originalImage.base64Encoded
    }
}


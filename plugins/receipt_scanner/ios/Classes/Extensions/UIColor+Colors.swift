//
//  UIColor+Colors.swift
//  Runner
//
//  Created by Marc Wiggerman on 07/05/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import UIKit

extension UIColor {
    class var cbsPurple: UIColor {
        return UIColor(red: 0.690, green: 0.055, blue: 0.498, alpha: 1.0)
    }
}


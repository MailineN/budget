#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint receipt_scanner.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'receipt_scanner'
  s.version          = '0.0.1'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '10.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'

  s.static_framework = true
  # Custom Dependencies
  # s.dependency 'WeScan', '>= 0.9'
  # s.dependency 'WeScanHBS', '>= 0.9' #, :git => 'https://github.com/MarcW1g/WeScan.git'

  s.dependency 'WeScanHBS', '>= 0.9'
  s.dependency 'MaterialComponents/ActivityIndicator'
  s.dependency 'GoogleMLKit/TextRecognition'
end

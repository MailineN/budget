
import 'package:receipt_scanner/models/quality.dart';
import 'package:receipt_scanner/models/text_observation.dart';
import 'package:tuple/tuple.dart';

// Class to store a collection of TextObservations representing a line
class ProductGroup {
  List<TextObservation> products;
  List<Tuple2<TextObservation, double>> prices;
  Tuple2<TextObservation, double> mainPrice;
  List<Quality> qualityAssessments = [];
  VisionRect productArea;

  ProductGroup() {
    this.products = [];
    this.prices = [];
  }

  void addProduct(TextObservation product) {
    this.products.add(product);
  }

  void addPrice(TextObservation price, double value) {
    this.prices.add(new Tuple2(price, value));
  }

  bool empty() {
    return this.products.length == 0 && this.prices.length == 0;
  } 

  void determinePrice(String textDirection) {
    // Determines the main price of the row by looking at the
    // locations of the found text containing price amounts

    if (this.prices.length == 0) {
      this.mainPrice = null;
      return;
    }

    this.prices.sort((a, b) => a.item1.normalizedRect.xPos.compareTo(b.item1.normalizedRect.xPos));
    
    // Pick the price based on the text direction
    switch (textDirection) {
      case "rl":
        this.mainPrice = this.prices.first;
        break;
      default:
        this.mainPrice = this.prices.last;
        break;
    }
  }

  TextObservation getMainProduct() {
    if (products.length > 0) {
      return products[0];
    }
    return null;
  }

  double getMainPrice() {
    if (this.mainPrice != null) {
      return this.mainPrice.item2;
    }
    return 0.0;
  }

  // Validates the quality of the individual product group
  void determineProductGroupQuality() {
    // Check if there is a main price
    if (this.mainPrice == null) qualityAssessments.add(Quality.missingPrice);

    // Check if there is a main product tile
    if (this.products.length == 0) qualityAssessments.add(Quality.missingTitle);

    // (and if it is of sufficent length & quality)
    bool oneTitleIsValid = false;
    for (final product in this.products) {
      if (product.text.length >= QualityConstants.minimumTitleLenght) oneTitleIsValid = true;
    }
    if (!oneTitleIsValid) qualityAssessments.add(Quality.missingTitle);

    // Check if the start and end of the product are not far apart - uses the area
    if (this.productArea != null) {
      if (this.productArea.size.height > QualityConstants.maxVerticalDistanceBetweenTitleAndPrice) qualityAssessments.add(Quality.poorAnalysis);
      if (this.productArea.xMin > 0.5) {
        qualityAssessments.add(Quality.oneSided);
      } else if (this.productArea.xMax < 0.5) {
        qualityAssessments.add(Quality.oneSided);
      }
    }

    // If we did not find any concerns, we add the label indicating a good scan
    if (qualityAssessments.length == 0) qualityAssessments.add(Quality.good);
  }

  void determineArea() {
    List<VisionRect> boundingBoxes = products.map((i) => i.normalizedRect).toList();
    VisionRect finalBoundingBox = VisionRect.union(boundingBoxes);
    this.productArea = finalBoundingBox;
  }
}

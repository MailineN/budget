package nl.cbs.householdbudgetsurvey.receipt_scanner;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import nl.cbs.householdbudgetsurvey.receipt_scanner.model.MessageEvent;
import nl.cbs.householdbudgetsurvey.receipt_scanner.model.ScanResult;
import nl.cbs.householdbudgetsurvey.receipt_scanner.model.VisionResult;
import com.github.chrisbanes.photoview.PhotoView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;

import org.greenrobot.eventbus.EventBus;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class ReceiptPreviewActivity extends AppCompatActivity {
    private static final String TAG = "DocScanner::Preview";

    public static Mat croppedGrayImage;
    public static Mat croppedThresholdedImage;
    public static Boolean shouldShowThresholded;
    public static Mat originalImage;

    ImageButton submitReceiptImage;
    ImageButton enableDisableFilterButton;
    ImageButton prevButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_preview_activity);

        // Get the views
        PhotoView croppedImageView = findViewById(R.id.cropped_preview_image);

        submitReceiptImage = findViewById(R.id.submit_receipt_button);
        enableDisableFilterButton = findViewById(R.id.enable_disable_filter_button);
        prevButton = findViewById(R.id.back_to_crop_button);
        ProgressBar progressBar = findViewById(R.id.scan_processing_progress_bar);
        progressBar.setVisibility(View.GONE);

        // Present the cropped receipt image
        this.setPreviewImage(croppedImageView, enableDisableFilterButton);

        submitReceiptImage.setOnClickListener((View v) -> {
            progressBar.setVisibility(View.VISIBLE);

            Mat selectedImage = croppedGrayImage;

            Bitmap croppedBitmap = matToBitmap(selectedImage);

            InputImage image = InputImage.fromBitmap(croppedBitmap, 0);
            TextRecognizer recognizer = TextRecognition.getClient();

            recognizer.process(image)
                .addOnSuccessListener(new OnSuccessListener<Text>() {
                    @Override
                    public void onSuccess(Text visionText) {
                        // Task completed successfully
                        VisionResult visionResults = processVisionResults(visionText, croppedBitmap);

                        Bitmap scaledOriginalBitmap =  prepareMatForBitmap(originalImage);
                        Bitmap scaledCroppedBitmap =  prepareMatForBitmap(selectedImage);

                        String jsonString = getJsonResults(scaledOriginalBitmap, scaledCroppedBitmap, visionResults);
                        returnToFlutterWithResultString(jsonString);
                    }
                })
                .addOnFailureListener(
                    new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            
                            // Task failed with an exception
                            returnToFlutterWithResultString("scanFailed");
                        }
                    });
        });

        enableDisableFilterButton.setOnClickListener((View view) -> {
            shouldShowThresholded = !shouldShowThresholded;
            this.setPreviewImage(croppedImageView, enableDisableFilterButton);
        });

        prevButton.setOnClickListener((View view) -> {
            finish();
        });
    }

    private Bitmap prepareMatForBitmap(Mat image) {
        Mat resizeImage = new Mat();
        double aspectRatioScaling = (double) image.height() / (double) image.width();
        Size szOg = new Size(1080, (int) (1080.0 *  aspectRatioScaling));

        Imgproc.resize(image, resizeImage, szOg);
        Bitmap bitmap = matToBitmap(resizeImage);

        return bitmap;
    }

    private void setPreviewImage(PhotoView imageView, ImageButton filterButton) {
        if (imageView == null || filterButton == null) return;


        

        if (croppedGrayImage != null) {
                    presentImageMat(croppedGrayImage, imageView);
                    filterButton.setImageResource(R.drawable.photo_filter);
        }
    }

    private void returnToFlutterWithResultString(String results) {
        Intent i = new Intent(ReceiptPreviewActivity.this, CameraView.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();

        EventBus.getDefault().post(new MessageEvent(results));
    }

    private void presentImageMat(Mat imageMat, PhotoView imageView) {
        // Present the cropped receipt image
        Bitmap croppedBitmap = matToBitmap(imageMat);
        imageView.setImageBitmap(croppedBitmap);
    }

    private Bitmap matToBitmap(Mat imageMat) {
        Bitmap croppedBitmap = Bitmap.createBitmap(imageMat.cols(), imageMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(imageMat, croppedBitmap);
        return croppedBitmap;
    }


    private String getJsonResults(Bitmap originalBitmap, Bitmap croppedBitmap, VisionResult visionResults) {
        Gson gson = new Gson();
        ScanResult scanResult = new ScanResult(visionResults, originalBitmap, croppedBitmap);
        return gson.toJson(scanResult);
    }


    private VisionResult processVisionResults(Text results, Bitmap sourceImage) {
        ArrayList<VisionResult.TextObservation> textObservations = new ArrayList<VisionResult.TextObservation>();
        double imageWidth = sourceImage.getWidth();
        double imageHeight = sourceImage.getHeight();


        for (Text.TextBlock block : results.getTextBlocks()) {
            for (Text.Line line : block.getLines()) {
                Rect boundingBox = line.getBoundingBox();
                String text = line.getText();

                VisionResult.Rect normalizedBoundingBox = normalizeRect(boundingBox, imageWidth, imageHeight);
                VisionResult.TextObservation obs = new VisionResult.TextObservation(text, -1.0, normalizedBoundingBox);
                textObservations.add(obs);
            }
        }

        return new VisionResult(textObservations);
    }

    private VisionResult.Rect normalizeRect(Rect sourceRect, double spaceWidth, double spaceHeight) {
        double newLeft = ((double) sourceRect.left / spaceWidth);
        double newTop = ((double) sourceRect.top / spaceHeight);
        double newRight = ((double) sourceRect.right / spaceWidth);
        double newBottom = ((double) sourceRect.bottom / spaceHeight);
        return new VisionResult.Rect(newLeft, newRight, newTop, newBottom);
    }
}

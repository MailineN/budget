package nl.cbs.householdbudgetsurvey.receipt_scanner.model;

import java.util.ArrayList;

public class VisionResult {
    public ArrayList<TextObservation> textObservations;

    public VisionResult(ArrayList<TextObservation> textObservations) {
        this.textObservations = textObservations;
    }

    // Required child classes
    public static class TextObservation {
        public String text;
        public double confidence;
        public Rect normalizedRect;

        public TextObservation(String text, double confidence, Rect normalizedRect) {
            this.text = text;
            this.confidence = confidence;
            this.normalizedRect = normalizedRect;
        }
    }

    public static class Rect {
        public double xPos;
        public double yPos;
        public Size size;

        public Rect(double left, double right, double top, double bottom) {
            double width = right - left;
            double height = bottom - top;
            Size size = new Size(width, height);

            this.xPos = left;
            this.yPos = top;
            this.size = size;
        }
    }

    public static class Size {
        public double width;
        public double height;

        public Size(double width, double height) {
            this.width = width;
            this.height = height;
        }
    }
}

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';

import 'core/controller/start_app.dart';
import 'core/controller/util/responsive_ui.dart';
import 'core/model/color_pallet.dart';
import 'core/model/international.dart';
import 'core/state/configuration.dart';
import 'core/state/translations.dart';
import 'features/filter_drawer/state/filter.dart';
import 'features/menu/menu.dart';
import 'features/para_data/para_data_scoped_model.dart';
import 'features/questionnaire/start_questionnaire_scoped_model.dart';
import 'screens/onboarding/welcome_screen.dart';
import 'screens/receipt_list/state/receipt_list_state.dart';
import 'screens/settings/state/settings_state.dart';

void main() async {
  await configureApp();
  final initialized = await isInitialized();
  runApp(GlobalWidget(initialized));
}

class GlobalWidget extends StatelessWidget {
  final bool userIsInitialized;

  const GlobalWidget(this.userIsInitialized);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<LanguageSetting>(
      model: LanguageSetting(),
      child: ScopedModelDescendant<LanguageSetting>(builder: (_, __, ___) {
        return ScopedModel<Configuration>(
          model: Configuration(),
          child: ScopedModelDescendant<Configuration>(builder: (_, __, configuration) {
            return ScopedModel<ParaDataScopedModel>(
              model: ParaDataScopedModel(configuration.paradata),
              child: ScopedModel<SettingState>(
                model: SettingState(),
                child: ScopedModel<FilterState>(
                  model: FilterState(GlobalKey<ScaffoldState>()),
                  child: ScopedModel<StartQuestionnaireModel>(
                    model: StartQuestionnaireModel(),
                    child: ScopedModel<ReceiptListState>(
                      model: ReceiptListState(),
                      child: Builder(
                        builder: (context) {
                          return MaterialApp(
                            navigatorObservers: [ParaDataScopedModel.of(context).observer()],
                            builder: (BuildContext context, Widget child) {
                              initializeUIParemeters(context);
                              return MediaQuery(
                                data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                                child: child,
                              );
                            },
                            debugShowCheckedModeBanner: false,
                            localizationsDelegates: const [
                              GlobalMaterialLocalizations.delegate,
                              GlobalWidgetsLocalizations.delegate,
                            ],
                            supportedLocales: International.locales(),
                            theme: ThemeData(primarySwatch: primarySwatchColor),
                            home: userIsInitialized ? Menu() : WelcomeScreen(),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        );
      }),
    );
  }
}

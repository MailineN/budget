import 'receipt_product.dart';

class Products {
  double discountAmount = 0;
  double discountPercentage = 0;
  List<ReceiptProduct> content = [];

  Products(this.discountAmount, this.discountPercentage, this.content);

  Products.empty();

  double getTotalPrice() {
    var _price = 0.0;
    for (final _product in content) {
      _price += _product.getTotalPrice();
    }
    return (_price - discountAmount) / 100 * (100 - discountPercentage);
  }

  void addProduct(ReceiptProduct product) {
    content.add(product);
  }

  void removeProduct(ReceiptProduct product) {
    content.remove(product);
  }

  void duplicateProduct(ReceiptProduct product) {
    final newProduct = ReceiptProduct(
      name: product.name,
      price: product.price,
      category: product.category,
      count: product.count,
      isReturn: product.isReturn,
    );
    content.add(newProduct);
  }
}

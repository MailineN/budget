import 'package:flutter/material.dart';

import '../controller/util/responsive_ui.dart';
import '../model/color_pallet.dart';
import '../state/translations.dart';

class NoReceiptImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/no_items_found_icon.png',
              height: 85.0 * y,
              width: 85.0 * x,
            ),
            SizedBox(height: 10.0 * y),
            Text(
              Translations.textStatic('noExpensesFound', 'Spending'),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: ColorPallet.midGray,
                fontWeight: FontWeight.w700,
                fontSize: 14.0 * f,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

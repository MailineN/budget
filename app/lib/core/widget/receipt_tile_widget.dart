import 'dart:io';

import 'package:flutter/material.dart';

import '../../features/para_data/para_data_scoped_model.dart';
import '../../screens/manual_entry/controller/add_receipt.dart';
import '../../screens/manual_entry/controller/delete_receipt.dart';
import '../../screens/manual_entry/controller/duplicate_receipt.dart';
import '../../screens/manual_entry/manual_entry_screen.dart';
import '../../screens/receipt_list/controller/product_controller.dart';
import '../../screens/receipt_list/controller/util/receipt_util.dart';
import '../../screens/receipt_list/state/receipt_list_state.dart';
import '../controller/util/currency_formatter.dart';
import '../controller/util/responsive_ui.dart';
import '../model/color_pallet.dart';
import '../model/receipt.dart';
import '../model/receipt_product.dart';
import '../state/translations.dart';
import 'product_tile_widget.dart';

class ReceiptTileWidget extends StatefulWidget {
  final Receipt receipt;

  const ReceiptTileWidget(this.receipt);

  @override
  _ReceiptTileWidgetState createState() => _ReceiptTileWidgetState();
}

class _ReceiptTileWidgetState extends State<ReceiptTileWidget> {
  ReceiptListState receiptListState;
  void showMySnackBar(Receipt receipt) async {
    final snackBar = SnackBar(
      duration: const Duration(seconds: 10),
      content: Text(Translations.textStatic('transactionRemoved', 'Manual_Entry')),
      action: SnackBarAction(
        textColor: ColorPallet.primaryColor,
        label: Translations.textStatic('undo', 'Manual_Entry'),
        onPressed: () async {
          await addReceipt(receiptListState, receipt);
          receiptListState.notify();
        },
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    receiptListState = ReceiptListState.of(context);
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 6.0 * x, vertical: 6.0 * y),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(5.0 * x, 1.0 * y), blurRadius: 2.0 * x, spreadRadius: 3.0 * x)],
          ),
          child: Theme(
            data: ThemeData(colorScheme: ColorScheme.fromSwatch().copyWith(secondary: ColorPallet.primaryColor)),
            child: ExpansionTile(
              title: Row(
                children: <Widget>[
                  SizedBox(
                    width: 145 * x,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.receipt.store.name,
                          style: TextStyle(fontSize: 18 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700),
                        ),
                        if (widget.receipt.store.category != '')
                          Text(
                            widget.receipt.store.category,
                            style: TextStyle(fontSize: 14 * f, color: ColorPallet.midGray, fontWeight: FontWeight.w600),
                          ),
                      ],
                    ),
                  ),
                  if (widget.receipt.imagePath == null)
                    Container()
                  else
                    Icon(
                      Icons.camera_alt,
                      color: ColorPallet.darkTextColor,
                      size: 24 * x,
                    ),
                  if (widget.receipt.imagePath == null) Container() else SizedBox(width: 10 * x),
                  Expanded(child: Container()),
                  Text(
                    widget.receipt.products.getTotalPrice().toStringAsFixed(2).addCurrencyFormat(),
                    style: TextStyle(fontSize: 16.5 * f, color: ColorPallet.pink, fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              leading: ReceiptUtil().getStoreIcon(widget.receipt.store.category),
              children: <Widget>[
                SizedBox(height: 5 * y),
                SizedBox(
                  height: 65 * y,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      InkWell(
                        onTap: () async {
                          await ParaDataScopedModel.of(context).onTap('InkWell', 'deleteReceipt');
                          await deleteReceipt(context, widget.receipt);
                          showMySnackBar(widget.receipt);
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('delete', 'Manual_Entry'),
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                              settings: const RouteSettings(name: 'ManualEntryScreen'),
                              builder: (context) => ManualEntryScreen(widget.receipt, true),
                            ),
                          );
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.mode_edit,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('modify', 'Manual_Entry'),
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await ParaDataScopedModel.of(context).onTap('InkWell', 'duplicateReceipt');
                          await duplicateReceipt(context, widget.receipt);
                          ReceiptListState.of(context).notify();
                        },
                        child: Container(
                          width: 115 * x,
                          height: 25 * y,
                          decoration: BoxDecoration(
                            color: ColorPallet.darkTextColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.content_copy,
                                color: Colors.white,
                                size: 16 * x,
                              ),
                              Text(Translations.textStatic('duplicate', 'Manual_Entry'),
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 13 * f)),
                              SizedBox(width: 1 * y),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 5 * y),
                if (widget.receipt.imagePath == null)
                  Container()
                else
                  File(widget.receipt.imagePath).existsSync() ? Image.file(File(widget.receipt.imagePath)) : Container(),
                FutureBuilder<List<ReceiptProduct>>(
                  future: ProductController().getProducts(widget.receipt.id),
                  builder: (context, products) {
                    if (products?.data == null) {
                      return Container();
                    }
                    return Column(
                      children: products.data.map((ReceiptProduct product) {
                        return ProductTileWidget(product);
                      }).toList(),
                    );
                  },
                ),
                //TODO: re-implement abroad, online, and discount status widgets
                SizedBox(height: 5 * y),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

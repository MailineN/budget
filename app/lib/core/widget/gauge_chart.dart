import 'dart:math';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class GaugeChart extends StatelessWidget {
  final List<charts.Series<dynamic, String>> seriesList;
  final bool animate;

  const GaugeChart(this.seriesList, {this.animate = false});

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(seriesList,
        animate: animate, defaultRenderer: charts.ArcRendererConfig(arcWidth: 15, startAngle: 4 / 5 * pi, arcLength: 7 / 5 * pi));
  }
}

class GaugeSegment {
  final String segment;
  final int size;
  final charts.Color color;

  GaugeSegment(this.segment, this.size, this.color);
}

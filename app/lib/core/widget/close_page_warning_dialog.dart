import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../controller/util/responsive_ui.dart';
import '../model/color_pallet.dart';
import '../state/translations.dart';

class ClosingScreenWarning extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      title: Container(
        color: ColorPallet.pink,
        padding: EdgeInsets.symmetric(horizontal: 20 * x, vertical: 20 * y),
        child: Row(
          children: <Widget>[
            Text(
              translations.text('warning'),
              style: TextStyle(fontSize: 20 * f, fontWeight: FontWeight.w500, color: Colors.white),
            ),
          ],
        ),
      ),
      content: Text(translations.text('removalWarningText'), style: TextStyle(fontSize: 17 * f, fontWeight: FontWeight.w400, color: ColorPallet.darkTextColor)),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
              statusBarColor: ColorPallet.primaryColor,
            ));

            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          child: Text(translations.text('delete'), style: const TextStyle(color: ColorPallet.pink)),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(translations.text('cancel'), style: const TextStyle(color: ColorPallet.pink)),
        )
      ],
    );
  }
}

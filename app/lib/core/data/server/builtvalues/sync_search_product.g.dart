// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_product.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchProduct> _$syncSearchProductSerializer =
    new _$SyncSearchProductSerializer();

class _$SyncSearchProductSerializer
    implements StructuredSerializer<SyncSearchProduct> {
  @override
  final Iterable<Type> types = const [SyncSearchProduct, _$SyncSearchProduct];
  @override
  final String wireName = 'SyncSearchProduct';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncSearchProduct object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'product',
      serializers.serialize(object.product,
          specifiedType: const FullType(String)),
      'productCategory',
      serializers.serialize(object.productCategory,
          specifiedType: const FullType(String)),
      'productCode',
      serializers.serialize(object.productCode,
          specifiedType: const FullType(String)),
      'lastAdded',
      serializers.serialize(object.lastAdded,
          specifiedType: const FullType(int)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncSearchProduct deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchProductBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'product':
          result.product = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productCategory':
          result.productCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productCode':
          result.productCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastAdded':
          result.lastAdded = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchProduct extends SyncSearchProduct {
  @override
  final String product;
  @override
  final String productCategory;
  @override
  final String productCode;
  @override
  final int lastAdded;
  @override
  final int count;

  factory _$SyncSearchProduct(
          [void Function(SyncSearchProductBuilder) updates]) =>
      (new SyncSearchProductBuilder()..update(updates)).build();

  _$SyncSearchProduct._(
      {this.product,
      this.productCategory,
      this.productCode,
      this.lastAdded,
      this.count})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        product, 'SyncSearchProduct', 'product');
    BuiltValueNullFieldError.checkNotNull(
        productCategory, 'SyncSearchProduct', 'productCategory');
    BuiltValueNullFieldError.checkNotNull(
        productCode, 'SyncSearchProduct', 'productCode');
    BuiltValueNullFieldError.checkNotNull(
        lastAdded, 'SyncSearchProduct', 'lastAdded');
    BuiltValueNullFieldError.checkNotNull(count, 'SyncSearchProduct', 'count');
  }

  @override
  SyncSearchProduct rebuild(void Function(SyncSearchProductBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchProductBuilder toBuilder() =>
      new SyncSearchProductBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchProduct &&
        product == other.product &&
        productCategory == other.productCategory &&
        productCode == other.productCode &&
        lastAdded == other.lastAdded &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, product.hashCode), productCategory.hashCode),
                productCode.hashCode),
            lastAdded.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncSearchProduct')
          ..add('product', product)
          ..add('productCategory', productCategory)
          ..add('productCode', productCode)
          ..add('lastAdded', lastAdded)
          ..add('count', count))
        .toString();
  }
}

class SyncSearchProductBuilder
    implements Builder<SyncSearchProduct, SyncSearchProductBuilder> {
  _$SyncSearchProduct _$v;

  String _product;
  String get product => _$this._product;
  set product(String product) => _$this._product = product;

  String _productCategory;
  String get productCategory => _$this._productCategory;
  set productCategory(String productCategory) =>
      _$this._productCategory = productCategory;

  String _productCode;
  String get productCode => _$this._productCode;
  set productCode(String productCode) => _$this._productCode = productCode;

  int _lastAdded;
  int get lastAdded => _$this._lastAdded;
  set lastAdded(int lastAdded) => _$this._lastAdded = lastAdded;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  SyncSearchProductBuilder();

  SyncSearchProductBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _product = $v.product;
      _productCategory = $v.productCategory;
      _productCode = $v.productCode;
      _lastAdded = $v.lastAdded;
      _count = $v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchProduct other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncSearchProduct;
  }

  @override
  void update(void Function(SyncSearchProductBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncSearchProduct build() {
    final _$result = _$v ??
        new _$SyncSearchProduct._(
            product: BuiltValueNullFieldError.checkNotNull(
                product, 'SyncSearchProduct', 'product'),
            productCategory: BuiltValueNullFieldError.checkNotNull(
                productCategory, 'SyncSearchProduct', 'productCategory'),
            productCode: BuiltValueNullFieldError.checkNotNull(
                productCode, 'SyncSearchProduct', 'productCode'),
            lastAdded: BuiltValueNullFieldError.checkNotNull(
                lastAdded, 'SyncSearchProduct', 'lastAdded'),
            count: BuiltValueNullFieldError.checkNotNull(
                count, 'SyncSearchProduct', 'count'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

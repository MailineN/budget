// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_store.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchStore> _$syncSearchStoreSerializer =
    new _$SyncSearchStoreSerializer();

class _$SyncSearchStoreSerializer
    implements StructuredSerializer<SyncSearchStore> {
  @override
  final Iterable<Type> types = const [SyncSearchStore, _$SyncSearchStore];
  @override
  final String wireName = 'SyncSearchStore';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncSearchStore object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'storeName',
      serializers.serialize(object.storeName,
          specifiedType: const FullType(String)),
      'storeType',
      serializers.serialize(object.storeType,
          specifiedType: const FullType(String)),
      'lastAdded',
      serializers.serialize(object.lastAdded,
          specifiedType: const FullType(int)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncSearchStore deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchStoreBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'storeName':
          result.storeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'storeType':
          result.storeType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastAdded':
          result.lastAdded = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchStore extends SyncSearchStore {
  @override
  final String storeName;
  @override
  final String storeType;
  @override
  final int lastAdded;
  @override
  final int count;

  factory _$SyncSearchStore([void Function(SyncSearchStoreBuilder) updates]) =>
      (new SyncSearchStoreBuilder()..update(updates)).build();

  _$SyncSearchStore._(
      {this.storeName, this.storeType, this.lastAdded, this.count})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        storeName, 'SyncSearchStore', 'storeName');
    BuiltValueNullFieldError.checkNotNull(
        storeType, 'SyncSearchStore', 'storeType');
    BuiltValueNullFieldError.checkNotNull(
        lastAdded, 'SyncSearchStore', 'lastAdded');
    BuiltValueNullFieldError.checkNotNull(count, 'SyncSearchStore', 'count');
  }

  @override
  SyncSearchStore rebuild(void Function(SyncSearchStoreBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchStoreBuilder toBuilder() =>
      new SyncSearchStoreBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchStore &&
        storeName == other.storeName &&
        storeType == other.storeType &&
        lastAdded == other.lastAdded &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, storeName.hashCode), storeType.hashCode),
            lastAdded.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncSearchStore')
          ..add('storeName', storeName)
          ..add('storeType', storeType)
          ..add('lastAdded', lastAdded)
          ..add('count', count))
        .toString();
  }
}

class SyncSearchStoreBuilder
    implements Builder<SyncSearchStore, SyncSearchStoreBuilder> {
  _$SyncSearchStore _$v;

  String _storeName;
  String get storeName => _$this._storeName;
  set storeName(String storeName) => _$this._storeName = storeName;

  String _storeType;
  String get storeType => _$this._storeType;
  set storeType(String storeType) => _$this._storeType = storeType;

  int _lastAdded;
  int get lastAdded => _$this._lastAdded;
  set lastAdded(int lastAdded) => _$this._lastAdded = lastAdded;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  SyncSearchStoreBuilder();

  SyncSearchStoreBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _storeName = $v.storeName;
      _storeType = $v.storeType;
      _lastAdded = $v.lastAdded;
      _count = $v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchStore other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncSearchStore;
  }

  @override
  void update(void Function(SyncSearchStoreBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncSearchStore build() {
    final _$result = _$v ??
        new _$SyncSearchStore._(
            storeName: BuiltValueNullFieldError.checkNotNull(
                storeName, 'SyncSearchStore', 'storeName'),
            storeType: BuiltValueNullFieldError.checkNotNull(
                storeType, 'SyncSearchStore', 'storeType'),
            lastAdded: BuiltValueNullFieldError.checkNotNull(
                lastAdded, 'SyncSearchStore', 'lastAdded'),
            count: BuiltValueNullFieldError.checkNotNull(
                count, 'SyncSearchStore', 'count'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

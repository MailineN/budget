// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_push_search_store_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPushSearchStoreBody> _$syncPushSearchStoreBodySerializer =
    new _$SyncPushSearchStoreBodySerializer();

class _$SyncPushSearchStoreBodySerializer
    implements StructuredSerializer<SyncPushSearchStoreBody> {
  @override
  final Iterable<Type> types = const [
    SyncPushSearchStoreBody,
    _$SyncPushSearchStoreBody
  ];
  @override
  final String wireName = 'SyncPushSearchStoreBody';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncPushSearchStoreBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'searchStore',
      serializers.serialize(object.searchStore,
          specifiedType: const FullType(SyncSearchStore)),
    ];

    return result;
  }

  @override
  SyncPushSearchStoreBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPushSearchStoreBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'searchStore':
          result.searchStore.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchStore))
              as SyncSearchStore);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPushSearchStoreBody extends SyncPushSearchStoreBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;
  @override
  final SyncSync synchronisation;
  @override
  final SyncSearchStore searchStore;

  factory _$SyncPushSearchStoreBody(
          [void Function(SyncPushSearchStoreBodyBuilder) updates]) =>
      (new SyncPushSearchStoreBodyBuilder()..update(updates)).build();

  _$SyncPushSearchStoreBody._(
      {this.user,
      this.phone,
      this.syncOrder,
      this.synchronisation,
      this.searchStore})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        user, 'SyncPushSearchStoreBody', 'user');
    BuiltValueNullFieldError.checkNotNull(
        phone, 'SyncPushSearchStoreBody', 'phone');
    BuiltValueNullFieldError.checkNotNull(
        syncOrder, 'SyncPushSearchStoreBody', 'syncOrder');
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, 'SyncPushSearchStoreBody', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(
        searchStore, 'SyncPushSearchStoreBody', 'searchStore');
  }

  @override
  SyncPushSearchStoreBody rebuild(
          void Function(SyncPushSearchStoreBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPushSearchStoreBodyBuilder toBuilder() =>
      new SyncPushSearchStoreBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPushSearchStoreBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder &&
        synchronisation == other.synchronisation &&
        searchStore == other.searchStore;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, user.hashCode), phone.hashCode), syncOrder.hashCode),
            synchronisation.hashCode),
        searchStore.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPushSearchStoreBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder)
          ..add('synchronisation', synchronisation)
          ..add('searchStore', searchStore))
        .toString();
  }
}

class SyncPushSearchStoreBodyBuilder
    implements
        Builder<SyncPushSearchStoreBody, SyncPushSearchStoreBodyBuilder> {
  _$SyncPushSearchStoreBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchStoreBuilder _searchStore;
  SyncSearchStoreBuilder get searchStore =>
      _$this._searchStore ??= new SyncSearchStoreBuilder();
  set searchStore(SyncSearchStoreBuilder searchStore) =>
      _$this._searchStore = searchStore;

  SyncPushSearchStoreBodyBuilder();

  SyncPushSearchStoreBodyBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _user = $v.user.toBuilder();
      _phone = $v.phone.toBuilder();
      _syncOrder = $v.syncOrder;
      _synchronisation = $v.synchronisation.toBuilder();
      _searchStore = $v.searchStore.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPushSearchStoreBody other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncPushSearchStoreBody;
  }

  @override
  void update(void Function(SyncPushSearchStoreBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPushSearchStoreBody build() {
    _$SyncPushSearchStoreBody _$result;
    try {
      _$result = _$v ??
          new _$SyncPushSearchStoreBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: BuiltValueNullFieldError.checkNotNull(
                  syncOrder, 'SyncPushSearchStoreBody', 'syncOrder'),
              synchronisation: synchronisation.build(),
              searchStore: searchStore.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();

        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'searchStore';
        searchStore.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncPushSearchStoreBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

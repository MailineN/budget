import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_image.dart';
import 'sync_phone.dart';
import 'sync_product.dart';
import 'sync_sync.dart';
import 'sync_transaction.dart';
import 'sync_user.dart';

part 'sync_push_receipt_body.g.dart';

abstract class SyncPushReceiptBody implements Built<SyncPushReceiptBody, SyncPushReceiptBodyBuilder> {
  static Serializer<SyncPushReceiptBody> get serializer => _$syncPushReceiptBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncTransaction get transaction;
  BuiltList<SyncProduct> get products;
  SyncImage get image;

  factory SyncPushReceiptBody([Function(SyncPushReceiptBodyBuilder b) updates]) = _$SyncPushReceiptBody;

  SyncPushReceiptBody._();

  factory SyncPushReceiptBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}

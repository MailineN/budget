// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_image.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncImage> _$syncImageSerializer = new _$SyncImageSerializer();

class _$SyncImageSerializer implements StructuredSerializer<SyncImage> {
  @override
  final Iterable<Type> types = const [SyncImage, _$SyncImage];
  @override
  final String wireName = 'SyncImage';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncImage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'base64image',
      serializers.serialize(object.base64image,
          specifiedType: const FullType(String)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncImage deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncImageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'base64image':
          result.base64image = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncImage extends SyncImage {
  @override
  final String base64image;
  @override
  final String transactionID;

  factory _$SyncImage([void Function(SyncImageBuilder) updates]) =>
      (new SyncImageBuilder()..update(updates)).build();

  _$SyncImage._({this.base64image, this.transactionID}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        base64image, 'SyncImage', 'base64image');
    BuiltValueNullFieldError.checkNotNull(
        transactionID, 'SyncImage', 'transactionID');
  }

  @override
  SyncImage rebuild(void Function(SyncImageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncImageBuilder toBuilder() => new SyncImageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncImage &&
        base64image == other.base64image &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, base64image.hashCode), transactionID.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncImage')
          ..add('base64image', base64image)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncImageBuilder implements Builder<SyncImage, SyncImageBuilder> {
  _$SyncImage _$v;

  String _base64image;
  String get base64image => _$this._base64image;
  set base64image(String base64image) => _$this._base64image = base64image;

  String _transactionID;
  String get transactionID => _$this._transactionID;
  set transactionID(String transactionID) =>
      _$this._transactionID = transactionID;

  SyncImageBuilder();

  SyncImageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _base64image = $v.base64image;
      _transactionID = $v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncImage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncImage;
  }

  @override
  void update(void Function(SyncImageBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncImage build() {
    final _$result = _$v ??
        new _$SyncImage._(
            base64image: BuiltValueNullFieldError.checkNotNull(
                base64image, 'SyncImage', 'base64image'),
            transactionID: BuiltValueNullFieldError.checkNotNull(
                transactionID, 'SyncImage', 'transactionID'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

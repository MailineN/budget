import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_phone_info.g.dart';

abstract class SyncPhoneInfo implements Built<SyncPhoneInfo, SyncPhoneInfoBuilder> {
  static Serializer<SyncPhoneInfo> get serializer => _$syncPhoneInfoSerializer;

  int get id;
  int get phoneId;
  String get key;
  String get value;

  factory SyncPhoneInfo([Function(SyncPhoneInfoBuilder b) updates]) = _$SyncPhoneInfo;

  SyncPhoneInfo._();

  factory SyncPhoneInfo.newInstance(String key, String value) {
    return SyncPhoneInfo((b) => b
      ..id = -1
      ..phoneId = -1
      ..key = key
      ..value = value);
  }
  factory SyncPhoneInfo.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}

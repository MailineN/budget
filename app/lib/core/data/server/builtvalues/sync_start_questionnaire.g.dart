// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_start_questionnaire.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncStartQuestionnaire> _$syncStartQuestionnaireSerializer =
    new _$SyncStartQuestionnaireSerializer();

class _$SyncStartQuestionnaireSerializer
    implements StructuredSerializer<SyncStartQuestionnaire> {
  @override
  final Iterable<Type> types = const [
    SyncStartQuestionnaire,
    _$SyncStartQuestionnaire
  ];
  @override
  final String wireName = 'SyncStartQuestionnaire';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncStartQuestionnaire object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'results',
      serializers.serialize(object.results,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncStartQuestionnaire deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncStartQuestionnaireBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'results':
          result.results = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncStartQuestionnaire extends SyncStartQuestionnaire {
  @override
  final String results;

  factory _$SyncStartQuestionnaire(
          [void Function(SyncStartQuestionnaireBuilder) updates]) =>
      (new SyncStartQuestionnaireBuilder()..update(updates)).build();

  _$SyncStartQuestionnaire._({this.results}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        results, 'SyncStartQuestionnaire', 'results');
  }

  @override
  SyncStartQuestionnaire rebuild(
          void Function(SyncStartQuestionnaireBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncStartQuestionnaireBuilder toBuilder() =>
      new SyncStartQuestionnaireBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncStartQuestionnaire && results == other.results;
  }

  @override
  int get hashCode {
    return $jf($jc(0, results.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncStartQuestionnaire')
          ..add('results', results))
        .toString();
  }
}

class SyncStartQuestionnaireBuilder
    implements Builder<SyncStartQuestionnaire, SyncStartQuestionnaireBuilder> {
  _$SyncStartQuestionnaire _$v;

  String _results;
  String get results => _$this._results;
  set results(String results) => _$this._results = results;

  SyncStartQuestionnaireBuilder();

  SyncStartQuestionnaireBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _results = $v.results;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncStartQuestionnaire other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncStartQuestionnaire;
  }

  @override
  void update(void Function(SyncStartQuestionnaireBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncStartQuestionnaire build() {
    final _$result = _$v ??
        new _$SyncStartQuestionnaire._(
            results: BuiltValueNullFieldError.checkNotNull(
                results, 'SyncStartQuestionnaire', 'results'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

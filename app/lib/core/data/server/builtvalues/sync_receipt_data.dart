import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_image.dart';
import 'sync_product.dart';
import 'sync_sync.dart';
import 'sync_transaction.dart';

part 'sync_receipt_data.g.dart';

abstract class SyncReceiptData implements Built<SyncReceiptData, SyncReceiptDataBuilder> {
  static Serializer<SyncReceiptData> get serializer => _$syncReceiptDataSerializer;

  SyncSync get synchronisation;
  SyncTransaction get transaction;
  BuiltList<SyncProduct> get products;
  SyncImage get image;

  factory SyncReceiptData([Function(SyncReceiptDataBuilder b) updates]) = _$SyncReceiptData;

  SyncReceiptData._();

  factory SyncReceiptData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}

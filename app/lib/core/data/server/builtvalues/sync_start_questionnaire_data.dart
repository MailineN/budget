import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_start_questionnaire.dart';
import 'sync_sync.dart';

part 'sync_start_questionnaire_data.g.dart';

abstract class SyncStartQuestionnaireData implements Built<SyncStartQuestionnaireData, SyncStartQuestionnaireDataBuilder> {
  static Serializer<SyncStartQuestionnaireData> get serializer => _$syncStartQuestionnaireDataSerializer;

  SyncSync get synchronisation;
  SyncStartQuestionnaire get syncStartQuestionnaire;

  factory SyncStartQuestionnaireData([Function(SyncStartQuestionnaireDataBuilder b) updates]) = _$SyncStartQuestionnaireData;

  SyncStartQuestionnaireData._();

  factory SyncStartQuestionnaireData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}

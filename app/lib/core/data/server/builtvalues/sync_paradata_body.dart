import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import '../../../../features/para_data/para_data.dart';
import 'serializers.dart';
import 'sync_phone.dart';
import 'sync_sync.dart';
import 'sync_user.dart';

part 'sync_paradata_body.g.dart';

abstract class SyncParadataBody implements Built<SyncParadataBody, SyncParadataBodyBuilder> {
  static Serializer<SyncParadataBody> get serializer => _$syncParadataBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  BuiltList<ParaData> get paradatas;

  factory SyncParadataBody([Function(SyncParadataBodyBuilder b) updates]) = _$SyncParadataBody;

  SyncParadataBody._();

  factory SyncParadataBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}

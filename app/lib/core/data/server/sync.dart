import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:budget_onderzoek/core/data/database/tables/questionnaire_export.dart';
import 'package:budget_onderzoek/core/data/server/builtvalues/sync_start_questionnaire.dart';
import 'package:budget_onderzoek/core/data/server/builtvalues/sync_start_questionnaire_body.dart';
import 'package:built_collection/built_collection.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';

import '../../../features/para_data/para_data.dart';
import '../../../features/para_data/para_data_table.dart';
import '../../../screens/search/controller/add_search_suggestion.dart';
import '../../state/translations.dart';
import '../database/database_helper.dart';
import '../database/tables/receipt.dart';
import '../database/tables/receipt_product.dart';
import 'builtvalues/sync_image.dart';
import 'builtvalues/sync_paradata_body.dart';
import 'builtvalues/sync_phone.dart';
import 'builtvalues/sync_phone_info.dart';
import 'builtvalues/sync_product.dart';
import 'builtvalues/sync_pull_body.dart';
import 'builtvalues/sync_push_receipt_body.dart';
import 'builtvalues/sync_push_search_product_body.dart';
import 'builtvalues/sync_push_search_store_body.dart';
import 'builtvalues/sync_receipt_data.dart';
import 'builtvalues/sync_register_body.dart';
import 'builtvalues/sync_register_data.dart';
import 'builtvalues/sync_search_product.dart';
import 'builtvalues/sync_search_product_data.dart';
import 'builtvalues/sync_search_store.dart';
import 'builtvalues/sync_search_store_data.dart';
import 'builtvalues/sync_sync.dart';
import 'builtvalues/sync_transaction.dart';
import 'builtvalues/sync_user.dart';
import 'sync_db.dart';

const backendIsActive = true;

class Synchronise {
  static String getUrlBase() {
    if (LanguageSetting.key == 'sl') {
      return 'https://hbs-test.azurewebsites.net';
    }

    if (LanguageSetting.key == 'hu') {
      return 'https://hbs.calvus.xyz';
    }

    if (LanguageSetting.key == 'fi') {
      return 'https://tk-t-hbs-backend-cont.azurewebsites.net';
    } else {
      // return 'http://192.168.1.43:8000';
      // return 'https://tijdbestedingsonderzoek.test.cbs.nl';
      return 'https://budgetonderzoek.ontwikkel.cbs.nl';
      // return 'http://192.168.43.100:8000';
    }
  }

  static Uri getUrlPushQuestionnaire() {
    return Uri.parse('${getUrlBase()}/budget/data/pushquestionnaire');
  }

  static Uri getUrlPushReceipt() {
    return Uri.parse('${getUrlBase()}/budget/data/pushreceipt');
  }

  static Uri getUrlPullReceipt() {
    return Uri.parse('${getUrlBase()}/budget/data/pullreceipt');
  }

  static Uri getUrlPushProduct() {
    return Uri.parse('${getUrlBase()}/budget/data/pushproduct');
  }

  static Uri getUrlPullProduct() {
    return Uri.parse('${getUrlBase()}/budget/data/pullproduct');
  }

  static Uri getUrlPushStore() {
    return Uri.parse('${getUrlBase()}/budget/data/pushstore');
  }

  static Uri getUrlPullStore() {
    return Uri.parse('${getUrlBase()}/budget/data/pullstore');
  }

  static Uri getUrlRegister() {
    return Uri.parse('${getUrlBase()}/budget/phone/register');
  }

  static Uri getUrlPushParaData() {
    return Uri.parse('${getUrlBase()}/budget/data/pushparadata');
  }

  static bool isBackendActive() {
    return backendIsActive;
  }

  static Future<void> synchronise() async {
    if (!backendIsActive) {
      return;
    }

    final _syncDatabase = SyncDatabase();
    final syncId = await _syncDatabase.getSyncId();

    if (syncId.userName != '' && syncId.phoneName != '') {
      final user = SyncUser.newInstance(syncId.userName, syncId.userPassword);
      final phone = SyncPhone.newInstance(syncId.phoneName);
      await pullAllReceipts(user, phone);
      await pullAllSearchProducts(user, phone);
      await pullAllSearchStores(user, phone);
      await pushAllDataTypes(user, phone);
      await pushStartQuestionnaireResults(_syncDatabase, user, phone);
    }
  }

  //pull and push all datatypes

  static Future<void> pullAllReceipts(SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataReceiptType);
      var syncData = await pullOneReceipt(user, phone, syncOrder);

      if (syncData.synchronisation.dataIdentifier == null || syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (syncData.transaction.receiptLocation != '') {
          final image = imageName(syncData.transaction.receiptLocation);
          final receiptLocation = await imagePath(image);
          syncData = syncData.rebuild((b) => b.transaction = syncData.transaction.rebuild((b) => b.receiptLocation = receiptLocation).toBuilder());
        }

        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData.synchronisation.rebuild((b) => b.syncOrder = 0).toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(dataReceiptType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await ReceiptTable().insertFromBackend(syncData.transaction);
            for (final prod in syncData.products) {
              await ReceiptProductTable().insertFromBackend(prod);
            }
          }
          if (syncData.transaction.receiptLocation != '') {
            final imageBytes = base64.decode(syncData.image.base64image);
            final file = File(syncData.transaction.receiptLocation);
            await file.writeAsBytes(imageBytes);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            await _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            // await TransactionDatabase.deleteTransaction(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await ReceiptTable().insertFromBackend(syncData.transaction);
              for (final prod in syncData.products) {
                await ReceiptProductTable().insertFromBackend(prod);
              }
              if (syncData.transaction.receiptLocation != '') {
                final imageBytes = base64.decode(syncData.image.base64image);
                final file = File(syncData.transaction.receiptLocation);
                await file.writeAsBytes(imageBytes);
              }
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataReceiptType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchProducts(SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataProductType);
      var syncData = await pullOneSearchProduct(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == null || syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData.synchronisation.rebuild((b) => b.syncOrder = 0).toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(dataProductType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertSearchProduct(syncData.searchProduct);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            await _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            await _syncDatabase.deleteSearchProduct(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertSearchProduct(syncData.searchProduct);
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataProductType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchStores(SyncUser user, SyncPhone phone) async {
    var act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      var syncOrder = await _syncDatabase.getSyncOrder(dataStoreType);
      var syncData = await pullOneSearchStore(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == null || syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData.synchronisation.rebuild((b) => b.syncOrder = 0).toBuilder());
        final oldSyncSync = await _syncDatabase.getSyncSync(dataStoreType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertSearchStore(syncData.searchStore);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            await _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            await _syncDatabase.deleteSearchStore(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertSearchStore(syncData.searchStore);
            }
          }
        }
        await _syncDatabase.updateSyncOrder(dataStoreType, syncOrder);
      }
    }
  }

  static Future<void> pushAllDataTypes(SyncUser user, SyncPhone phone) async {
    var lastPushed = -1;
    var attempt = 0;

    var act = true;
    var previousParadataPushSucceeded = true;

    final _syncDatabase = SyncDatabase();

    while (act) {
      final syncRegisterData = await getPhonePushStatus(user, phone);
      final synchronisation = await _syncDatabase.getNextSyncSyncToPush(syncRegisterData.phone.syncOrder);

      if (lastPushed != synchronisation.syncOrder) {
        lastPushed = synchronisation.syncOrder;
        attempt = 1;
      } else if (attempt < 5) {
        attempt = attempt + 1;
        sleep(const Duration(milliseconds: 100));
      } else {
        break;
      }

      if (synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (synchronisation.dataType == dataReceiptType) {
          await pushOneReceipt(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataProductType) {
          await pushOneSearchProduct(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataStoreType) {
          await pushOneSearchStore(synchronisation, user, phone);
        } else if (synchronisation.dataType == dataTypeParadata) {
          previousParadataPushSucceeded = false;
        }
      }
    }

    SyncSync syncParadata;
    if (previousParadataPushSucceeded) {
      syncParadata = await _syncDatabase.getFirstSyncSync(dataTypeParadata);
      if (syncParadata.dataIdentifier != '') {
        //delete paradata < syncParadata.syncTime
        final _table = ParaDataTable(await DatabaseHelper.instance.database);
        await _table.delete(syncParadata.syncTime);
      }
    }
    await _syncDatabase.maintainSyncSync(dataTypeParadata, '001');
    syncParadata = await _syncDatabase.getFirstSyncSync(dataTypeParadata);
    await pushAllParadata(syncParadata, user, phone);
  }

  //push one datatype

  static Future pushOneReceipt(SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncTransaction transaction;
    final products = <SyncProduct>[];
    SyncImage image;

    if (synchronisation.action != deleted) {
      final List<Map<String, dynamic>> trans = await ReceiptTable.syncQuery(synchronisation.dataIdentifier);

      if (trans.isNotEmpty) {
        transaction = SyncTransaction.fromJson(trans.first);
        final prods = await ReceiptProductTable.syncQuery(synchronisation.dataIdentifier);

        for (final product in prods) {
          products.add(SyncProduct.fromJson(product));
        }

        image = SyncImage((b) => b
          ..transactionID = transaction.transactionID
          ..base64image = '');

        if (transaction.receiptLocation != '') {
          final imageFile = File(transaction.receiptLocation);
          final List<int> imageBytes = imageFile.readAsBytesSync();
          image = image.rebuild((b) => b.base64image = base64.encode(imageBytes));
          transaction = transaction.rebuild((b) => b.receiptLocation = imageName(transaction.receiptLocation));
        }
      } else {
        transaction = SyncTransaction.newInstance(synchronisation.dataIdentifier);
        image = SyncImage((b) => b
          ..transactionID = transaction.transactionID
          ..base64image = '');
      }

      final body = SyncPushReceiptBody((b) => b
        ..user = user.toBuilder()
        ..phone = phone.toBuilder()
        ..syncOrder = synchronisation.syncOrder
        ..synchronisation = synchronisation.toBuilder()
        ..transaction = transaction.toBuilder()
        ..products = ListBuilder<SyncProduct>(products)
        ..image = image.toBuilder());

      await post(getUrlPushReceipt(), body: jsonEncode(body));
    } else {
      transaction = SyncTransaction.newInstance(synchronisation.dataIdentifier);
      image = SyncImage((b) => b
        ..transactionID = transaction.transactionID
        ..base64image = '');

      final body = SyncPushReceiptBody((b) => b
        ..user = user.toBuilder()
        ..phone = phone.toBuilder()
        ..syncOrder = synchronisation.syncOrder
        ..synchronisation = synchronisation.toBuilder()
        ..transaction = transaction.toBuilder()
        ..products = ListBuilder<SyncProduct>(products)
        ..image = image.toBuilder());

      await post(getUrlPushReceipt(), body: jsonEncode(body));
    }
  }

  static Future pushOneSearchProduct(SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncSearchProduct searchProduct;
    if (synchronisation.action != deleted) {
      final searchProducts = await SearchSuggestions.querySearchProduct(synchronisation.dataIdentifier);
      searchProduct = SyncSearchProduct.fromJson(searchProducts.first);
    } else {
      searchProduct = SyncSearchProduct.newInstance(synchronisation.dataIdentifier);
    }

    final body = SyncPushSearchProductBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..searchProduct = searchProduct.toBuilder());

    await post(getUrlPushProduct(), body: jsonEncode(body));
  }

  static Future pushOneSearchStore(SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncSearchStore searchStore;
    if (synchronisation.action != deleted) {
      final searchStores = await SearchSuggestions.querySearchStore(synchronisation.dataIdentifier);
      searchStore = SyncSearchStore.fromJson(searchStores.first);
    } else {
      searchStore = SyncSearchStore.newInstance(synchronisation.dataIdentifier);
    }

    final body = SyncPushSearchStoreBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..searchStore = searchStore.toBuilder());

    await post(getUrlPushStore(), body: jsonEncode(body));
  }

  //pull one datatype

  static Future<SyncReceiptData> pullOneReceipt(SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final response = await post(getUrlPullReceipt(), body: jsonEncode(body));
    return SyncReceiptData.fromJson(json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncSearchProductData> pullOneSearchProduct(SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final response = await post(getUrlPullProduct(), body: jsonEncode(body));
    return SyncSearchProductData.fromJson(json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncSearchStoreData> pullOneSearchStore(SyncUser user, SyncPhone phone, int syncOrder) async {
    final body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final response = await post(getUrlPullStore(), body: jsonEncode(body));
    return SyncSearchStoreData.fromJson(json.decode(response.body) as Map<String, dynamic>);
  }

  //file path functions

  static String imageName(String path) {
    const prefix = '/Pictures/flutter_test/';
    var i = path.indexOf(prefix);
    if (i < 0) {
      return path;
    } else {
      i = i + prefix.length;
      return path.substring(i);
    }
  }

  static Future<String> imagePath(String name) async {
    final extDir = await getApplicationDocumentsDirectory();
    final dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    return '$dirPath/$name';
  }

  //register phone and get phone push status

  static Future<SyncRegisterData> getPhonePushStatus(SyncUser user, SyncPhone phone) async {
    final phoneInfos = <SyncPhoneInfo>[];
    final body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));
    final response = await post(getUrlRegister(), body: jsonEncode(body));
    return SyncRegisterData.fromJson(json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncRegisterData> registerNewPhone(String userName, String userPassword, String phoneName, List<SyncPhoneInfo> phoneInfos) async {
    final user = SyncUser.newInstance(userName, userPassword);
    final phone = SyncPhone.newInstance(phoneName);
    final body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));
    try {
      final response = await post(getUrlRegister(), body: jsonEncode(body)).timeout(const Duration(seconds: 5));
      return SyncRegisterData.fromJson(json.decode(response.body) as Map<String, dynamic>);
    } catch (e) {
      return null;
    }
  }

  // Paradata

  static Future pushAllParadata(SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    final paradatas = <ParaData>[];

    if (synchronisation.action != deleted) {
      final _table = ParaDataTable(await DatabaseHelper.instance.database);
      final _paradatas = await _table.select();

      for (final _paradata in _paradatas) {
        paradatas.add(ParaData.fromJson(_paradata));
      }
    }

    final body = SyncParadataBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..paradatas = ListBuilder<ParaData>(paradatas));

    await post(getUrlPushParaData(), body: jsonEncode(body));
  }

  static Future pushStartQuestionnaireResults(SyncDatabase syncDatabase, SyncUser user, SyncPhone phone) async {
    await syncDatabase.maintainSyncSync(dataQuestionnaire, '001');
    final synchronisation = await syncDatabase.getFirstSyncSync(dataQuestionnaire);

    final questionnaireExport = await getQuestionnaireExport();
    final startQuestionnaire = SyncStartQuestionnaire((b) => b..results = questionnaireExport);

    final body = SyncStartQuestionnaireBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..startQuestionnaire = startQuestionnaire.toBuilder());

    await post(getUrlPushQuestionnaire(), body: jsonEncode(body));
  }
}

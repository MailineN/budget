import '../../../state/translations.dart';
import '../database_helper.dart';

class TableProduct {
  Future<List<Map<String, dynamic>>> query(String productName, String category) async {
    final db = await DatabaseHelper.instance.database;
    return db.query('tblProduct_${LanguageSetting.tablePreference}', where: 'product = ? AND coicop = ?', whereArgs: [productName.toLowerCase(), category]);
  }

  Future<void> insert(String productName, String coicop) async {
    final db = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['code'] = '12.8.0.0';
    data['frequency'] = '0';
    data['product'] = productName.toLowerCase();
    data['coicop'] = coicop;
    await db.insert('tblProduct_${LanguageSetting.tablePreference}', data);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../widget/animated_focus_light.dart';
import '../widget/content_target.dart';
import '../widget/target_focus.dart';
import '../widget/tutorial_coach_mark.dart';

List<TargetFocus> targets = [];

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(GlobalKey keyButton1, GlobalKey keyButton2, GlobalKey keyButton3, GlobalKey keyButton4, GlobalKey keyButton5) {
  if (targets.isEmpty) {
    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('mainInsights1', 'Overview_T'),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('mainInsights2', 'Overview_T'),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 290 * y),
              Column(
                children: <Widget>[
                  Image.asset(
                    'assets/images/tab_symbol.png',
                    height: 100 * y,
                  ),
                  SizedBox(height: 20 * y),
                  Text(
                    Translations.textStatic('swipeToNavigate', 'Overview_T'),
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20 * f),
                  ),
                ],
              ),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('mainInsights3', 'Overview_T'),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('mainInsights4', 'Overview_T'),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton3,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('mainInsights5', 'Overview_T'),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('mainInsights6', 'Overview_T'),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                Translations.textStatic('mainInsights7', 'Overview_T'),
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  Translations.textStatic('mainInsights8', 'Overview_T'),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton5,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10 * y),
                  Text(
                    Translations.textStatic('mainInsights9', 'Overview_T'),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      Translations.textStatic('mainInsights10', 'Overview_T'),
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 30 * y),
                ],
              ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  );
  showGestureLogo = true;
  TutorialCoachMark(
    context,
    targets: targets,
    colorShadow: ColorPallet.darkTextColor,
    textSkip: Translations.textStatic('skip', '?'),
    textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
    alignSkip: Alignment.topLeft,
    opacityShadow: .98,
    clickSkip: () {
      showGestureLogo = false;
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    finish: () {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    clickTarget: (TargetFocus target) {
      showGestureLogo = false;
    },
  ).show();
}

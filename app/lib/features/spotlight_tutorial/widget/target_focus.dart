import 'package:flutter/widgets.dart';

import 'animated_focus_light.dart';
import 'content_target.dart';
import 'target_position.dart';

class TargetFocus {
  TargetFocus({
    this.identify,
    this.keyTarget,
    this.targetPosition,
    this.contents,
    this.shape,
  }) : assert(keyTarget != null || targetPosition != null);

  final List<ContentTarget> contents;
  final dynamic identify;
  final GlobalKey keyTarget;
  final ShapeLightFocus shape;
  final TargetPosition targetPosition;

  @override
  String toString() {
    return 'TargetFocus{identify: $identify, keyTarget: $keyTarget, targetPosition: $targetPosition, contents: $contents, shape: $shape}';
  }
}

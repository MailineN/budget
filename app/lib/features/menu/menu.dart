import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:questionnaire/questionnaire.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../core/controller/user_progress.dart';
import '../../core/controller/util/date.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/progress_lists.dart';
import '../../core/model/receipt.dart';
import '../../core/state/configuration.dart';
import '../../core/state/translations.dart';
import '../../screens/camera_entry/ocr_plugin/receipt_scanner.dart';
import '../../screens/insights/insights_screen.dart';
import '../../screens/manual_entry/manual_entry_screen.dart';
import '../../screens/overview/overview_screen.dart';
import '../../screens/receipt_list/receipt_list_screen.dart';
import '../../screens/settings/settings_screen.dart';
import '../../screens/settings/state/settings_state.dart';
import '../filter_drawer/state/filter.dart';
import '../para_data/para_data_name.dart';
import '../para_data/para_data_scoped_model.dart';
import '../questionnaire/questionnaire_mixin.dart';
import '../questionnaire/start_questionnaire_scoped_model.dart';

GlobalKey keyButton4 = GlobalKey();
Translations translations;

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> with TickerProviderStateMixin, CreateQuestionnaireMixin, OnQuestionnaireCompleteMixin, WidgetsBindingObserver {
  static const List<IconData> icons = [
    Icons.camera_alt,
    Icons.keyboard,
  ];

  AnimationController _controller;
  ProgressLists progressLists;
  bool _showingQuestionnaire = false;
  int _currentScreenIndex = 0;
  StartQuestionnaireModel _startQuestionnaireModel;
  final List<Widget> _screens = [
    OverviewScreen(keyButton4),
    ReceiptListScreen(),
    Container(),
    InsightsScreen(),
    SettingsScreen(),
  ];

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    ParaDataScopedModel.of(context).onTap('didChangeAppLifecycleState', '$state');
  }

  Future<void> _showStartQuestionnaire(BuildContext context) async {
    final language = LanguageSetting.of(context).language ?? '';
    _startQuestionnaireModel = StartQuestionnaireModel.of(context);
    final _hasEntry = await _startQuestionnaireModel.hasStartQuestionnaireEntry();
    if (!_hasEntry && !_showingQuestionnaire) {
      setState(() {
        _showingQuestionnaire = true;
      });

      print('First Day in Experiment [${progressLists.daysInExperiment.first}] and last day in experiment [${progressLists.daysInExperiment.last}]');

      createQuestionnaire(language, DateUtil.questionnaireDateRepresentation(progressLists.daysInExperiment.first),
          DateUtil.questionnaireDateRepresentation(progressLists.daysInExperiment.last));

      await Navigator.of(context).push(MaterialPageRoute(
        settings: const RouteSettings(name: 'StartQuestionnairePage'),
        builder: (BuildContext context) {
          return StartQuestionnairePage(
            questionList: questionList,
            colorMap: colorList,
            textMap: textList,
            delegate: this,
          );
        },
      ));
    }
  }

  static void changeStatusBarColor() {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: ColorPallet.primaryColor,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    changeStatusBarColor();
    WidgetsBinding.instance.addObserver(this);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );

    UserProgressController().getProgressLists().then((value) => progressLists = value);
  }

  @override
  void onQuestionnaireComplete(Map<int, Object> answers) {
    print('_MenuState: OnQuestionnaireComplete [$answers]');

    answers.forEach((questionNumber, item) {
      switch (item.runtimeType) {
        case NormalResult:
          _startQuestionnaireModel.insertNormal(item);
          break;

        case SingleChoiceResult:
          _startQuestionnaireModel.insertSingle(item);
          break;

        case MultiChoiceResult:
          _startQuestionnaireModel.insertMulti(item);
          break;

        case PersonInfoResult:
          _startQuestionnaireModel.insertPersonInfo(item);
          break;

        default:
          throw Exception('Unsupported Type, not supported !');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Calendar');

    return ScopedModelDescendant<Configuration>(builder: (_, __, configuration) {
      if (configuration.questionnaire == QuestionnaireConfiguration.enabled) {
        if (LanguageSetting.key != 'fi') {
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            _showStartQuestionnaire(context);
          });
        }
      }
      return WillPopScope(
        onWillPop: () => Future.value(false),
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: ColorPallet.primaryColor,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 10 * y,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      key: keyButton4,
                      height: 50.0 * y,
                      width: 56.0 * x,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              child: SafeArea(
                child: Scaffold(
                  resizeToAvoidBottomInset: false,
                  floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
                  floatingActionButton: SizedBox(
                    height: 230 * y,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: List.generate(icons.length, (int index) {
                        final child = Container(
                          height: 70.0 * y,
                          width: 56.0 * x,
                          alignment: FractionalOffset.topCenter,
                          child: ScaleTransition(
                            scale: CurvedAnimation(
                              parent: _controller,
                              curve: Interval(0, 1.0 - index / icons.length / 2.0, curve: Curves.easeOut),
                            ),
                            child: FloatingActionButton(
                              heroTag: null,
                              backgroundColor: ColorPallet.pink,
                              onPressed: () async {
                                await _controller.reverse();
                                if (icons[index] == Icons.camera_alt) {
                                  await ParaDataScopedModel.of(context).onTap('FloatingActionButton', 'ReceiptOCR');
                                  await openReceiptScanner(context);
                                } else if (icons[index] == Icons.keyboard) {
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      settings: const RouteSettings(name: 'ManualEntryScreen'),
                                      builder: (context) => ManualEntryScreen(
                                        Receipt.empty(),
                                        false,
                                      ),
                                    ),
                                  );
                                }
                                _currentScreenIndex = 1;
                              },
                              child: Icon(icons[index], color: Colors.white, size: 26 * x),
                            ),
                          ),
                        );
                        return child;
                      }).toList()
                        ..add(
                          Container(
                            width: 69.0 * x,
                            height: 69.0 * y,
                            child: FittedBox(
                              child: FloatingActionButton(
                                heroTag: null,
                                onPressed: () {
                                  if (_controller.isDismissed) {
                                    _controller.forward();
                                  } else {
                                    _controller.reverse();
                                  }
                                },
                                child: AnimatedBuilder(
                                  animation: _controller,
                                  builder: (BuildContext context, Widget child) {
                                    return Transform(
                                      transform: Matrix4.rotationZ(_controller.value * 0.75 * math.pi),
                                      alignment: FractionalOffset.center,
                                      child: Icon(
                                        _controller.isDismissed ? Icons.add : Icons.add,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                    ),
                  ),
                  body: Container(
                    color: ColorPallet.primaryColor,
                    child: SafeArea(
                      child: Container(
                        color: Colors.white,
                        child: _screens[_currentScreenIndex],
                      ),
                    ),
                  ),
                  bottomNavigationBar: ScopedModelDescendant<SettingState>(builder: (context, child, model) {
                    return Theme(
                      data: Theme.of(context).copyWith(
                          canvasColor: Colors.white,
                          primaryColor: Colors.blue,
                          textTheme: Theme.of(context).textTheme.copyWith(caption: const TextStyle(color: Colors.grey))),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(0.0 * x, -2.2 * y), blurRadius: 1.7 * y, spreadRadius: 1.7 * y)
                          ],
                        ),
                        child: BottomNavigationBar(
                          elevation: 0,
                          iconSize: 30.0 * x,
                          onTap: (index) {
                            ParaDataScopedModel.of(context).onTap((_screens[index] as ParaDataName).name, 'openTab');
                            setState(() {
                              if (index != 2) {
                                FilterState.of(context).reset();
                                _currentScreenIndex = index;
                              }
                              _controller.reverse();
                            });
                          },
                          fixedColor: ColorPallet.primaryColor,
                          type: BottomNavigationBarType.fixed,
                          currentIndex: _currentScreenIndex,
                          items: [
                            {'icon': Icons.event_note, 'text': translations.text('overviewPage')},
                            {'icon': Icons.receipt, 'text': translations.text('expensesPage')},
                            {'icon': Icons.add, 'text': ''},
                            {'icon': Icons.equalizer, 'text': translations.text('insightsPage')},
                            {'icon': Icons.settings, 'text': translations.text('settings')},
                          ].map(
                            (Map<String, dynamic> values) {
                              return BottomNavigationBarItem(
                                icon: Icon(values['icon']),
                                // ignore: deprecated_member_use
                                title: Text(
                                  values['text'],
                                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13.0 * f),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}

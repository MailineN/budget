import 'dart:math';

import 'package:edit_distance/edit_distance.dart';

import 'string_extensions.dart';

double getEditDistance(String productName, String userInput, NormalizedStringDistance stringDistanceMetric) {
  var score = 0.0;

  productName = productName.toLowerCase().withoutDiacriticalMarks;
  userInput = userInput.toLowerCase().withoutDiacriticalMarks;

  if (userInput.contains(' ')) {
    score = stringDistanceMetric.normalizedDistance(userInput, productName);
  } else {
    final wordScores = <double>[];
    for (final productWord in productName.split(' ')) {
      final _wordScore = stringDistanceMetric.normalizedDistance(userInput, productWord);
      wordScores.add(_wordScore);
    }
    score = wordScores.reduce(min);
  }
  return score;
}

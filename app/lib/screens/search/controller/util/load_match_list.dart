import '../../../../core/data/database/database_helper.dart';
import '../../../../core/state/translations.dart';
import '../../model/match.dart';

List<SearchMatch> shopMatchList;

Future<List<SearchMatch>> getShopMatchList() async {
  if (shopMatchList == null) {
    shopMatchList = await loadShopMatchList();
    return shopMatchList;
  }
  return shopMatchList;
}

Future<List<SearchMatch>> loadShopMatchList() async {
  final db = await DatabaseHelper.instance.database;
  final List<Map<String, dynamic>> shopMap = await db.query(
    'tblShop_${LanguageSetting.tablePreference}',
  );
  final searchMatch = <SearchMatch>[];
  for (final dynamic row in shopMap) {
    searchMatch.add(
      SearchMatch(
        code: '',
        frequency: 1,
        name: row['shop'],
        category: row['shoptype'],
      ),
    );
  }
  return searchMatch;
}

List<SearchMatch> productMatchList;

Future<List<SearchMatch>> getProductMatchList() async {
  if (productMatchList == null) {
    productMatchList = await loadProductMatchList();
    return productMatchList;
  }
  return productMatchList;
}

Future<List<SearchMatch>> loadProductMatchList() async {
  final db = await DatabaseHelper.instance.database;
  final List<Map<String, dynamic>> shopMap = await db.query(
    'tblProduct_${LanguageSetting.tablePreference}',
  );
  final searchMatch = <SearchMatch>[];
  for (final dynamic row in shopMap) {
    searchMatch.add(
      SearchMatch(
        code: '',
        frequency: row['frequency'],
        name: row['product'],
        category: row['coicop'],
      ),
    );
  }
  return searchMatch;
}

import 'package:meta/meta.dart';

class SearchMatch {
  final String code;
  final int frequency;
  final String name;
  final String category;

  double editDistance;

  SearchMatch({
    @required this.code,
    @required this.frequency,
    @required this.name,
    @required this.category,
  });
}

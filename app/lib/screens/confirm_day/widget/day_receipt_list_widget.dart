import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/receipt.dart';
import '../../../core/widget/receipt_tile_widget.dart';
import '../controller/receipt_controller.dart';

class DayReceiptListWidget extends StatelessWidget {
  final DateTime date;

  const DayReceiptListWidget(this.date);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Receipt>>(
        future: ReceiptController().getReceipts(date),
        builder: (context, snapshot) {
          if (snapshot?.data == null) {
            return Container();
          }
          return Expanded(
            child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 6.0 * x, vertical: 6 * y),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return ReceiptTileWidget(snapshot.data[index]);
              },
            ),
          );
        });
  }
}

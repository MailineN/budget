import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

import '../../core/controller/user_progress.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/data/database/tables/receipt.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/enum/date_status.dart';
import '../../core/model/international.dart';
import '../../core/model/receipt.dart';
import '../../core/state/translations.dart';
import '../../features/filter_drawer/state/filter.dart';
import '../../features/para_data/para_data_name.dart';
import '../camera_entry/ocr_plugin/receipt_scanner.dart';
import '../manual_entry/manual_entry_screen.dart';
import '../receipt_list/state/receipt_list_state.dart';
import 'widget/day_receipt_list_widget.dart';

Translations translations;

class ConfirmDayScreen extends StatefulWidget with ParaDataName {
  final DateTime date;

  const ConfirmDayScreen(this.date);

  @override
  String get name => 'ConfirmDayScreen';

  @override
  _ConfirmDayScreenState createState() => _ConfirmDayScreenState();
}

class _ConfirmDayScreenState extends State<ConfirmDayScreen> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _colorTween;
  bool _showAnimation = false;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
    _colorTween = ColorTween(begin: ColorPallet.primaryColor, end: ColorPallet.lightGreen).animate(_animationController);
    super.initState();
  }

  Future<List<Receipt>> queryDay() {
    final startDateTimstamp = widget.date.millisecondsSinceEpoch;
    final endDateTimestamp = widget.date.add(const Duration(hours: 24)).millisecondsSinceEpoch;
    return ReceiptTable().query(startDate: startDateTimstamp, endDate: endDateTimestamp);
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Confirm_Day');
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${translations.text('expensesPage')} - ${DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey).format(widget.date)}',
          style: TextStyle(color: Colors.white, fontSize: 23 * f),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: ScopedModelDescendant<FilterState>(
        builder: (_context2, _child2, filterState) => ScopedModelDescendant<ReceiptListState>(
          builder: (context, child, receiptListState) => Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                    child: Stack(
                  children: <Widget>[
                    if (_showAnimation)
                      CheckMarkAnimation()
                    else
                      Column(
                        children: [
                          SizedBox(height: 70 * y),
                          DayReceiptListWidget(widget.date),
                        ],
                      ),
                    Positioned(
                      top: 15 * y,
                      child: _showAnimation == true
                          ? const SizedBox()
                          : SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(left: 8 * x),
                                    width: 200 * x,
                                    child: ElevatedButton.icon(
                                      style: ElevatedButton.styleFrom(
                                        primary: ColorPallet.primaryColor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8),
                                        ),
                                      ),
                                      icon: Icon(Icons.camera_alt, color: Colors.white, size: 20 * x),
                                      label: Text(
                                        translations.text('add'),
                                        style: TextStyle(color: Colors.white, fontSize: 18 * f, fontWeight: FontWeight.w600),
                                      ),
                                      onPressed: () async {
                                        await openReceiptScanner(context);
                                      },
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 8 * x, right: 8 * x),
                                    width: 200 * x,
                                    child: ElevatedButton.icon(
                                      style: ElevatedButton.styleFrom(
                                        primary: ColorPallet.primaryColor,
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                                      ),
                                      icon: Icon(Icons.keyboard, color: Colors.white, size: 20 * x),
                                      label: Text(
                                        translations.text('add'),
                                        style: TextStyle(color: Colors.white, fontSize: 18 * f, fontWeight: FontWeight.w600),
                                      ),
                                      onPressed: () {
                                        final receipt = Receipt.empty();
                                        receipt.date = widget.date;
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            settings: const RouteSettings(name: 'ManualEntryScreen'),
                                            builder: (context) => ManualEntryScreen(receipt, false),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ),
                    FutureBuilder<DateStatus>(
                        future: UserProgressController().getDateStatus(widget.date),
                        builder: (context, dateStatus) {
                          if (dateStatus == null) {
                            return Container();
                          }
                          return Positioned(
                            bottom: 20 * y,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    width: 380 * x,
                                    child: AnimatedBuilder(
                                      animation: _colorTween,
                                      builder: (context, child) => ElevatedButton.icon(
                                        onPressed: () async {
                                          if (_showAnimation) {
                                            Navigator.pop(context);
                                            return true;
                                          }
                                          if (dateStatus.data == DateStatus.canComplete) {
                                            await UserProgressController().setDateStatus(widget.date, true);
                                            setState(() {
                                              _showAnimation = true;
                                            });
                                            if (_animationController.status == AnimationStatus.completed) {
                                              await _animationController.reverse();
                                            } else {
                                              await _animationController.forward();
                                            }
                                            ReceiptListState.of(context).notify();
                                          } else {
                                            if (dateStatus.data == DateStatus.alreadyComplete) {
                                              Toast.show(translations.text('dayAlreadyCompleted'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                            } else {
                                              Toast.show(translations.text('youCanNotCompleteDays'), context,
                                                  duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                            }
                                          }
                                        },
                                        icon: Icon(
                                          Icons.check,
                                          color: _showAnimation ? Colors.transparent : Colors.white,
                                          size: 20 * x,
                                        ),
                                        label: Text(
                                          _showAnimation
                                              ? translations.text('ok')
                                              : dateStatus.data == DateStatus.alreadyComplete
                                                  ? translations.text('dayAlreadyCompleted')
                                                  : translations.text('completeDay'),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18 * f,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          primary: dateStatus.data == DateStatus.canComplete
                                              ? _colorTween.value
                                              : dateStatus.data == DateStatus.alreadyComplete
                                                  ? ColorPallet.lightGreen
                                                  : ColorPallet.lightGray,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        })
                  ],
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CheckMarkAnimation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 520 * y,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 270 * y,
            width: 270 * x,
            child: const FlareActor(
              'assets/images/CheckMark.flr',
              animation: 'Checkmark Appear',
            ),
          ),
          SizedBox(height: 20 * y),
          Text(
            translations.text('dayCompleted'),
            style: TextStyle(
              color: ColorPallet.lightGreen,
              fontSize: 30 * f,
              fontWeight: FontWeight.w800,
            ),
          ),
          SizedBox(height: 60 * y)
        ],
      ),
    );
  }
}

import 'dart:ui' as ui;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/data/database/database_helper.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/international.dart';
import '../../core/state/translations.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/para_data/para_data_scoped_model.dart';
import 'login_screen.dart';

Translations translations;

class WelcomeScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'WelcomeScreen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    initDatabase();
    initializeTranslations();

    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor, systemNavigationBarColor: ColorPallet.primaryColor),
    );
    final country = International.countryFromCode(ui.window.locale.languageCode);
    dropdownValue = country.name;
    LanguageSetting.of(context).language = country.languageId;
    LanguageSetting.tablePreference = country.id;
  }

  Future<void> _launchURL() async {
    final url = translations.text('url');
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void changeCountry(String country) {
    setState(() {
      dropdownValue = country;
    });
  }

  Future<void> initDatabase() async {
    await DatabaseHelper.instance.database;
  }

  Widget _countryIcon(String country) {
    final png = International.countryFromName(country).flagImage;
    if (png == '') {
      return Row(
        children: <Widget>[
          SizedBox(width: 30 * x, height: 110 * y),
          Text(
            country,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18 * f,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      );
    }
    return Row(
      children: <Widget>[
        SizedBox(width: 10 * x, height: 110 * y),
        Container(
            width: 26 * x,
            height: 26 * x,
            decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: ExactAssetImage('assets/images/$png')))),
        SizedBox(
          width: 10 * x,
        ),
        Text(
          country,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18 * f,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: ColorPallet.primaryColor,
        child: Column(
          children: <Widget>[
            SizedBox(height: 40 * y),
            SizedBox(
              height: 240 * y,
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0 * x),
                  child: Text(
                    translations.text('hbsLong2'),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 50 * f,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            SizedBox(height: 120 * y),
            Container(
              width: 310 * x,
              color: ColorPallet.primaryColor,
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: ColorPallet.primaryColor,
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    iconEnabledColor: Colors.white.withOpacity(0.8),
                    value: dropdownValue,
                    elevation: 0,
                    onChanged: (String newValue) {
                      setState(() {
                        //dropdownValue = newValue;
                        changeCountry(newValue);
                        LanguageSetting.of(context).language = International.countryFromName(newValue).languageId;
                        LanguageSetting.tablePreference = International.countryFromName(newValue).id;
                        ParaDataScopedModel.of(context).onTap('InkWell', 'changedCountrySetting:$newValue');
                      });
                    },
                    items: International.countries().map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                          value: value,
                          child: Container(
                            height: 45 * y,
                            decoration: const BoxDecoration(
                                color: ColorPallet.lightBlueWithOpacity,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                )),
                            //color: ColorPallet.lightBlueWithOpacity,
                            width: 260 * x,
                            margin: const EdgeInsets.all(5),
                            child: _countryIcon(value),
                          ));
                    }).toList(),
                  ),
                ),
              ),
            ),
            SizedBox(height: 67 * y),
            ButtonTheme(
              minWidth: 300.0 * x,
              height: 40.0 * y,
              child: ElevatedButton(
                onPressed: () {
                  if (dropdownValue != 'Choose a country') {
                    //TODO, replace true with: if(Synchronise.isBackendActive()){}
                    if (true) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: const RouteSettings(name: 'LoginScreen'),
                          builder: (context) => LoginScreen(),
                        ),
                      );
                    }
                    // else {
                    //   Login.validateInput(context, 'hbs###***###2019');
                    // }
                  }
                },
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(horizontal: 40 * x),
                  primary: dropdownValue != 'Choose a country' ? ColorPallet.lightGreen : ColorPallet.lightGray,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(11),
                  ),
                ),
                child: Text(
                  translations.text('start'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18 * f,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(child: Container()),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: translations.text('url'),
                  style: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 17.0 * f, decoration: TextDecoration.underline),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      ParaDataScopedModel.of(context).onTap('InkWell', 'launchUrl');
                      _launchURL();
                    },
                ),
                const TextSpan(text: '\n'),
              ]),
            ),
            SizedBox(height: 20 * y),
          ],
        ),
      ),
    );
  }
}

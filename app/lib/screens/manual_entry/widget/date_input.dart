import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/international.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton3 = GlobalKey();

class DateInput extends StatelessWidget {
  Future<void> selectDate(BuildContext context, ReceiptState receiptState) async {
    var picked = await showDatePicker(
      locale: International.languageFromId(LanguageSetting.key).locale,
      context: context,
      routeSettings: const RouteSettings(name: 'DatePicker'),
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(const Duration(days: 365)),
      lastDate: DateTime.now().add(const Duration(days: 365)),
    );
    picked = DateTime(picked.year, picked.month, picked.day);
    if (picked != null) {
      receiptState.receipt.date = picked;
      receiptState.notify();
    }
  }

  String getDateString(DateTime date, Translations translations) {
    final receiptDate = DateFormat('yyyy-MM-dd').format(date);
    final today = DateFormat('yyyy-MM-dd').format(DateTime.now());
    final yesterday = DateFormat('yyyy-MM-dd').format(DateTime.now().subtract(const Duration(days: 1)));
    final tomorrow = DateFormat('yyyy-MM-dd').format(DateTime.now().add(const Duration(days: 1)));

    if (receiptDate == today) {
      return translations.text('today');
    } else if (receiptDate == yesterday) {
      return translations.text('yesterday');
    } else if (receiptDate == tomorrow) {
      return translations.text('tomorrow');
    } else {
      return DateFormat('EEEE, d MMMM', International.languageFromId(LanguageSetting.key).localeKey).format(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return ScopedModelDescendant<ReceiptState>(
      builder: (context, child, receiptState) {
        return Stack(
          key: keyButton3,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
                  child: InkWell(
                    onTap: () async {
                      await selectDate(context, receiptState);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.today, color: ColorPallet.darkTextColor, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Text(
                          getDateString(receiptState.receipt.date, translations),
                          style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 48 * x,
              top: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
                color: Colors.white,
                child: Text('${translations.text('date')}*', style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f)),
              ),
            ),
          ],
        );
      },
    );
  }
}

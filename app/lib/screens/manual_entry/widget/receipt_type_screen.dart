import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../camera_entry/ocr_plugin/receipt_scanner.dart';

class ReceiptTypeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Camera_Entry');
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: ColorPallet.pink),
    );
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: 80 * y,
            color: ColorPallet.pink,
            child: Column(
              children: [
                SizedBox(height: 40 * y),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20 * x),
                    Text(
                      translations.text('receiptCategory'),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24 * f,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    InkWell(
                      onTap: () async {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 30 * x,
                      ),
                    ),
                    SizedBox(width: 20 * x),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 25 * y),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.85,
            child: Text(
              translations.text('receiptCategoryExplanation'),
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontSize: 16.0 * f,
                fontWeight: FontWeight.w600,
                height: 1.2 * y,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 22.0 * x),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CategoryButton(Icons.healing, translations.text('health'), 30 * x, '06.0.0.0.0'),
                      CategoryButton(Icons.phone, translations.text('communication'), 30 * x, '08.0.0.0.0'),
                      CategoryButton(FontAwesomeIcons.square, translations.text('otherCategory'), 29 * x, '12.0.0.0.0'),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CategoryButton(Icons.local_laundry_service, translations.text('householdExpenses'), 31 * x, '05.0.0.0.0'),
                      CategoryButton(Icons.house, translations.text('housingCost'), 31 * x, '04.0.0.0.0'),
                      CategoryButton(Icons.school, translations.text('education'), 31 * x, '10.0.0.0.0'),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CategoryButton(FontAwesomeIcons.tshirt, translations.text('clothing'), 28 * x, '03.0.0.0.0'),
                      CategoryButton(Icons.hotel, translations.text('restaurantAndHotel'), 31 * x, '11.0.0.0.0'),
                      CategoryButton(Icons.local_movies, translations.text('recreationAndCultur'), 30 * x, '09.0.0.0.0'),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CategoryButton(Icons.local_bar, translations.text('alcoholAndTobacco'), 31 * x, '02.0.0.0.0'),
                      CategoryButton(Icons.local_dining, translations.text('foodAndDrink'), 31 * x, '01.0.0.0.0'),
                      CategoryButton(Icons.local_taxi, translations.text('transport'), 31 * x, '07.0.0.0.0'),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 7 * y),
        ],
      ),
    );
  }
}

class CategoryButton extends StatelessWidget {
  const CategoryButton(this.iconData, this.text, this.iconSize, this.coicop);

  final IconData iconData;
  final double iconSize;
  final String coicop;
  final String text;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        final receiptCategory = ProductCategory(text, coicop);
        Navigator.pop(context, receiptCategory);
      },
      child: SizedBox(
        width: 100 * x,
        height: 90 * y,
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 5 * y),
              Icon(
                iconData,
                color: ColorPallet.darkTextColor,
                size: iconSize,
              ),
              SizedBox(height: 8 * y),
              Text(
                text,
                style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 13 * f, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

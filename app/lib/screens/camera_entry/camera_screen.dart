import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';

class CameraPage extends StatefulWidget {
  const CameraPage(this.isEditing);

  final bool isEditing;

  @override
  __CameraPageState createState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return __CameraPageState();
  }
}

class __CameraPageState extends State<CameraPage> {
  CameraController _controller;
  String _dirPath;

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    availableCameras().then((cameras) {
      _controller = CameraController(cameras[0], ResolutionPreset.high, enableAudio: false);
      _controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  void onTakePictureButtonPressed() {
    setState(() {});
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {});
        if (filePath != null) {
          setState(() {});
          print('Picture saved to $filePath');
          Navigator.pop(context, filePath);
        }
      }
    });
  }

  Future<String> takePicture() async {
    if (!_controller.value.isInitialized) {
      print('Error select camera first');
      return null;
    }
    final extDir = await getApplicationDocumentsDirectory();
    _dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(_dirPath).create(recursive: true);
    if (_controller.value.isTakingPicture) {
      // A capture is already pending, do nothing
      return null;
    }

    try {
      final xFile = await _controller.takePicture();
      return xFile.path;
    } on CameraException catch (e) {
      print(e);
      return null;
    }
  }

  Widget _cameraPreviewWidget() {
    if (_controller == null || !_controller.value.isInitialized) {
      return Container();
    } else {
      return AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: CameraPreview(_controller),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorPallet.pink,
        title: Row(
          children: <Widget>[
            Text(
              Translations(context, 'Camera').text('photographReceipt'),
              style: TextStyle(fontSize: 24 * f),
            ),
          ],
        ),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  child: SizedBox(
                    width: double.infinity,
                    child: _cameraPreviewWidget(),
                  ),
                ),
              ],
            ),
            Positioned(
              child: Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                  ),
                  Positioned(
                    bottom: 20,
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 70 * x,
                            height: 70 * y,
                            child: FittedBox(
                              child: FloatingActionButton(
                                backgroundColor: ColorPallet.pink,
                                onPressed: () {
                                  onTakePictureButtonPressed();
                                },
                                child: const Icon(Icons.camera_alt),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
